<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttributeToAcceptedAndCompletedVideoCallScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_call_schedule', function (Blueprint $table) {
            //
            $table->integer('accepted')->nullable()->change();
            $table->integer('completed')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_call_schedule', function (Blueprint $table) {
            //
        });
    }
}
