<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('params', function (Blueprint $table) {
            $table->id();
            $table->integer("user_id");
            $table->enum("playground", ['romance', 'networking', 'friends']);
            $table->string("bio", 350)->default("");
            $table->string("ages")->default("[18, 99]");
            $table->integer("distance")->default(1);
            $table->string("gender_industry", 300)->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('params');
    }
}
