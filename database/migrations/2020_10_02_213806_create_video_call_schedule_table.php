<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoCallScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_call_schedule', function (Blueprint $table) {
            $table->id();
            $table->integer("caller");
            $table->integer("receiver");
            $table->date("mdate");
            $table->string("mtime");
            $table->integer("accepted");
            $table->integer("completed");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_call_schedule');
    }
}
