<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string("firstname");
            $table->string("lastname");
            $table->string("phone", 20)->unique();
            $table->string('username', 50)->unique();
            $table->string('email', 50)->unique();
            $table->date("birthday");
            $table->integer("identify_id")->default(0);
            $table->string("cuisine_id")->nullable();
            $table->integer("astrological_id")->default(0);
            $table->string("industry_id")->nullable();
            $table->string("headshot")->nullable();
            $table->string("photos", 500)->nullable();
            $table->string('password');
            $table->tinyInteger("romance")->default(0);
            $table->tinyInteger("networking")->default(0);
            $table->tinyInteger("friends")->default(0);
            $table->string("activities", 500)->nullable();
            $table->string("conditions", 500)->nullable();
            $table->integer("marital")->default(0);
            $table->integer("spiritual")->default(0);
            $table->integer("alcohol")->default(0);
            $table->integer("tabacco")->default(0);
            $table->integer("friendly420")->default(0);
            $table->integer("children")->default(0);
            $table->integer("animal")->default(0);
            $table->string("position", 300);
            $table->string("country");
            $table->string("city");
            $table->text('push_tokens', 300)->nullable();
            $table->integer("register_completed")->default(0);
            $table->integer("membership")->default(0);
            $table->date("expire_at")->nullable();
            $table->integer("rewind")->default(0);
            $table->date("rewind_expire_at")->nullable();
            $table->integer("video")->default(0);
            $table->integer("trial_video")->default(0);
            $table->date("video_expire_at")->nullable();
            $table->integer("filter")->default(0);
            $table->date("filter_expire_at")->nullable();
            $table->integer("tossed")->default(0);
            $table->date("tossed_expire_at")->nullable();
            $table->integer("photo")->default(0);
            $table->date("photo_expire_at")->nullable();
            $table->integer("free_rewind")->default(2);
            $table->integer("blocked")->default(0);
            $table->text("block_users")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
