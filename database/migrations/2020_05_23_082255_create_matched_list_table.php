<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchedListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matched_list', function (Blueprint $table) {
            $table->id();
            $table->integer("user_id");
            $table->integer("tossed_id");
            $table->integer("toss_type")->default(0);
            $table->string("playground")->default("romance");
            $table->integer("is_new")->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matched_list');
    }
}
