<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'api\UserController@login');
Route::post('refresh_fcm', 'api\UserController@refresh_fcm');
Route::post('remove_fcm', 'api\UserController@remove_fcm');
Route::post('register_user', 'api\UserController@register');
Route::post('checkPhonenumberDuplicate', 'api\UserController@check_phonenumber');
Route::get('getGeneralResources', 'api\UserController@get_general_resources');
Route::get("getIndustry", "api\UserController@get_industry");
Route::post('checkValidUserName','api\UserController@check_valid_username');
Route::post("checkValidEmail", "api\UserController@check_valid_email");
Route::post("updateUserSetting", "api\UserController@update_user_setting");
Route::post("insertUserParams", "api\UserController@insert_matching_params");
Route::post("storePlaygrounds", "api\UserController@store_playgrounds");
Route::post("addUserImage", "api\UserController@add_user_image");
Route::post("deleteUserImage", "api\UserController@delete_user_image");
Route::post("updateCuisines", "api\UserController@update_cuisines");
Route::post("updateIndustries", "api\UserController@update_indusries");
Route::post("updateIdentifies", "api\UserController@update_identifies");
Route::post("updateMarital", "api\UserController@update_marital");
Route::post("updateSpiritual", "api\UserController@update_spiritual");
Route::post("updateAlcohol", "api\UserController@update_alcohol");
Route::post("updateTobacco", "api\UserController@update_tobacco");
Route::post("update420", "api\UserController@update_friend420");
Route::post("updateChildren", "api\UserController@update_children");
Route::post("updateAnimal", "api\UserController@update_animal");
Route::post("updateBio", "api\UserController@update_bio");
Route::post("updatePlayground", "api\UserController@update_playground");
Route::post("updateUsername", "api\UserController@update_username");
Route::post("updateDistance", "api\UserController@update_user_distance");
Route::post("updateAgeRange", "api\UserController@update_user_age_range");
Route::post("updatePhonenumber", "api\UserController@update_user_phone_number");
Route::post("updateUserIdentify", "api\UserController@update_user_identify");
Route::post("purchaseItem", "api\PaymentController@purchase_item");
Route::post("user/{id}/block", "api\UserController@block_user");
Route::get("user/{id}/blocks", "api\UserController@get_blocked_users");
Route::post("user/{id}/unblock", "api\UserController@unblock_user");

Route::post("deleteAccount", "api\UserController@delete_account");

// Match Algorithm 
Route::get("getMatches/{user_id}/{playground}", "api\MatchController@get_matches");
Route::get("getLikes/{user_id}/{playground}", "api\MatchController@get_likes");
Route::post("pass", "api\MatchController@pass_match");
Route::post("toss", "api\MatchController@toss_match");
Route::post("back", "api\MatchController@back_match");
Route::post("updateIsNew", "api\MatchController@update_is_new");
Route::get("searchMember","api\MatchController@search");

// For Message Functions
Route::get("getNewConnections/{user_id}/{playground}", "api\MessageController@get_new_connections");
Route::get("getReceiverPushToken/{username}", "api\UserController@get_receiver_push_token");
Route::get("getTwilioAccessToken/{identify}/{ruid}", "api\MessageController@get_twilio_access_token");
Route::post("setVideoCallSchedule", "api\MessageController@set_video_call_schedule");
Route::post("accpetVideoCallSchedule", "api\MessageController@accpept_video_call_schedule");
Route::post("suggestVideoCallWithNewTime", "api\MessageController@suggest_video_call_with_new_time");
Route::post("delineVideoCallSchedule", "api\MessageController@decline_video_call_schedule");
Route::post("scheduleNotification", "api\MessageController@video_chat_schedule_notification");

// For Company Affilate
Route::get("getCompanyTags", "api\CompanyController@get_all_tags");
Route::get("getCompanies", "api\CompanyController@get_companies");

Route::get("getUser/{username}/{myusername}", "api\UserController@get_user");
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Report User

Route::post("report", "api\ReportController@report");
