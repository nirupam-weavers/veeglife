<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
// User
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'UserController@index')->name('users');
Route::get('/user/{username}/profile', 'UserController@user')->name('user');
Route::delete("/user/{username}/remove",  'UserController@remove')->name('remove_user');
Route::get("/user/{username}/messages", "UserController@messages")->name("messages");
Route::post("/user/{user_id}/block", "UserController@block_user")->name("block");
Route::post("/user/{user_id}/unblock", "UserController@unblock_user")->name("unblock");

// Company
Route::resource("companies", CompanyController::class);
Route::resource("ctags", CompanyTagController::class);

Route::get("/companies/{id}/images", "CompanyController@get_company_images")->name("companies.images");
Route::put("/companies/{id}/image", "CompanyController@add_new_image")->name("companies.image");
Route::delete("/companies/{id}/images/{image}", "CompanyController@delete_company_image")->name("companies.delete_image");

// Reports

Route::get("/reports", "ReportController@index")->name("reports");
//Packages
Route::get("/packageitems", "PackageItemsController@index")->name("packageitems");
Route::post("/packageitems", "PackageItemsController@store")->name("packageitems.store");
Route::put('/packageitem/{package}','PackageItemsController@update');

Route::get("/packages", "PackagesController@index")->name("packages");
Route::post("/packages", "PackagesController@store")->name("packages.store");
