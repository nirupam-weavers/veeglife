@extends('layouts.app')
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-card card mb-3">
                    <div class="card-header w-100 justify-content-between">
                        <h4 class="card-title">Reports</h4>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped mb-3">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Reporter</th>
                                    <th>User</th>
                                    <th>Content</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($reports as $key => $report)
                                @if($report['user'] != NULL && $report['reporter'] != NULL)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $report['created_at']}}</td>
                                    <td><a href="{{ route('user', ['username'=>$report['reporter']['username']])}}">{{ $report['reporter']['username'] }}</a></td>
                                    <td><a href="{{ route('user', ['username' => $report['user']['username']]) }}">{{ $report['user']['username']}}</a></td>
                                    <td>{{ $report['content'] }}</td>
                                    <td>
                                        @if($report['user']['blocked'] == 0)
                                        <form method="POST" action="{{route('block', ['user_id'=>$report['user_id']])}}">
                                            @csrf
                                            <button class="btn btn-sm btn-danger btn_block_user">Block User</button>
                                        </form>
                                        @else
                                            Blocked
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection