@extends('layouts.app')
@section('content')

    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-card card">
                        <h4> <b> <u> Packages  </u>  </b> </h4>
                        <div class="card-header">
                            <h6> <b> Option 1: </b> &nbsp &nbsp (Photo, Tossed & Rewind)   </h6>
                        </div>
                    </div>

                    <div class="main-card card">
                        <div class="card-header">
                            <h6> <b> Option 2: </b> &nbsp &nbsp (Only Video)   </h6>
                        </div>
                    </div>

                    <div class="main-card card">
                        <div class="card-header">
                            <h6> <b> Option 3: </b> &nbsp &nbsp (Photo, Tossed, Rewind & Video)  </h6>
                        </div>
                        <br>
                        <form action="{{ route('packages.store') }}" method="post">
                            @csrf
                            <div>
                                <label for="staticEmail" class="col-sm-4 col-form-label"> <b> Choose any option from below: </b>  </label>
                            </div>
                            <br>
                            <div class="form-check ml-3">
                                <input class="form-check-input" type="radio" name="option" value="option1" >
                                <label class="form-check-label" for="exampleRadios1">
                                    Option 1
                                </label>
                            </div>

                            <br>

                            <div class="form-check ml-3">
                                <input class="form-check-input" type="radio" name="option" value="option2" >
                                <label class="form-check-label" for="exampleRadios2">
                                    Option 2
                                </label>
                            </div>
                            <br>

                            <div class="form-check ml-3">
                                <input class="form-check-input" type="radio" name="option" value="option3" >
                                <label class="form-check-label" for="exampleRadios3">
                                    Option 3
                                </label>
                            </div>
                            <br>



                            <button type="submit" class="btn btn-primary btn-sm ml-4"> Submit </button>
                        </form>
                    </div>

                </div>

            </div>
        </div>
    </div>
    @include('sweetalert::alert')

@endsection
