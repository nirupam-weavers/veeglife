@extends('layouts.app')
@section('content')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-card card mb-3">
                        <div class="card-header w-100 justify-content-between">
                            <h4 class="card-title">Package Items </h4>
                            <button class="btn btn-primary btn-sm" id="btn_add_company"><i class="fa fa-plus"></i> Add Package Item </button>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="mb-0 table table-striped company_table">
                                <thead>
                                <tr>
                                    <th>UserId</th>
                                    <th>Photo</th>
                                    <th>Tossed</th>
                                    <th>Filter</th>
                                    <th>Video</th>
                                    <th>Rewind</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($packages as $key => $package)
                                    <tr>
                                        <td> {{ $key + 1 }} </td>
                                        <td>{{ $package['photo'] }}</td>
                                        <td>{{ $package['tossed'] }}</td>
                                        <td>{{ $package['filter'] }}</td>
                                        <td>{{ $package['video'] }}</td>
                                        <td>{{ $package['rewind'] }}</td>
                                        <td style="width : 170px">
                                            <button class="btn btn-primary btn-sm edit_company" data-value='{{json_encode($package)}}'>Edit</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('sweetalert::alert')

@endsection


<div class="modal fade" id="add_company_modal" tabindex="-1" role="dialog" aria-labelledby="addTagModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- FORM to add package item  -->
            <form method="POST" action="{{route('packageitems.store')}}" >
                <div class="modal-header">
                    <h5 class="modal-title" id="addTagModalLabel">Set Values for all Users </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    @csrf
                    <div class="row">

                        <div class="col-sm-12 form-group">
                            <label class="control-label"> <b> Total Number of Users: </b>  </label>
                            {{ $userscount}}
                        </div>

                        <div class="col-sm-12 form-group">
                            <label class="control-label"> <b>Total Set Photos:</b> </label>
                            {{ $photocount}}
                        </div>

                        <div class="col-sm-12 form-group">
                            <label class="control-label"><b> Total Set Video: </b> </label>
                            {{ $videocount}}
                        </div>

                        <div class="col-sm-12 form-group">
                            <label class="control-label"> <b>Total Set Filters:</b> </label>
                            {{ $filtercount}}
                        </div>

                        <div class="col-sm-12 form-group">
                            <label class="control-label"><b>Total Set Tossed:</b> </label>
                            {{ $tossedcount}}
                        </div>

                        <div class="col-sm-12 form-group">
                            <label class="control-label"><b> Total Set Rewind: </b></label>
                            {{ $rewindcount}}
                        </div>

                        <div class="col-sm-12 form-group">
                            <label class="control-label"><b><u>TICK VALUES FROM BELOW TO SET: 1 </u>  </b></label>
                        </div>

                        <div class="col-sm-12 form-group d-flex-justify">
                            <div>
                                <input type="checkbox" name="photo" value=1>
                                <label> Photo </label>
                            </div>

                            <div>
                                <input type="checkbox" name="video" value=1>
                                <label> Video </label>
                            </div>

                            <div>
                                <input type="checkbox" name="filter" value=1>
                                <label> Filter </label>
                            </div>

                            <div>
                                <input type="checkbox" name="tossed" value=1>
                                <label> Tossed </label>
                            </div>

                            <div>
                                <input type="checkbox" name="rewind" value=1>
                                <label> Rewind </label>
                            </div>

                        </div>
                        <!-- End of group Selections -->

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add Package Item</button>
                </div>
            </form>
            <!-- End Form-Add Package Item -->
        </div>
    </div>
</div>

<div class="modal fade" id="edit_company_modal" tabindex="-1" role="dialog" aria-labelledby="addTagModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="" id="edit_company_form">
                @csrf
                @method('PUT')
                <div class="modal-header">
                    <h5 class="modal-title" id="addTagModalLabel">Edit Package Item </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-sm-12 form-group">
                            <label class="control-label">User Name </label>
                            <input type="text" name="username" class="form-control" required id="edit_username" disabled>
                        </div>

                        <div class="col-sm-12 form-group">
                            <label class="control-label">Email </label>
                            <input type="email" name="email" class="form-control" required id="edit_email" disabled>
                        </div>

                        <div class="col-sm-12 form-group">
                            <label class="control-label">City </label>
                            <input type="text" name="city" class="form-control" required id="edit_city" disabled>
                        </div>

                        <div class="col-sm-12 form-group">
                            <label class="control-label">Country </label>
                            <input type="text" name="country" class="form-control" required id="edit_country" disabled>
                        </div>

                        <div class="col-sm-12 form-group">
                            <label class="control-label">Phone </label>
                            <input type="text" name="phone" class="form-control" required id="edit_phone" disabled>
                        </div>


                        <div class="col-sm-12 form-group d-flex-justify">
                            <div>
                                <input type="checkbox" name="photo" value=1>
                                <label> Photo </label>
                            </div>

                            <div>
                                <input type="checkbox" name="video" value=1>
                                <label> Video </label>
                            </div>

                            <div>
                                <input type="checkbox" name="filter" value=1>
                                <label> Filter </label>
                            </div>

                            <div>
                                <input type="checkbox" name="tossed" value=1>
                                <label> Tossed </label>
                            </div>

                            <div>
                                <input type="checkbox" name="rewind" value=1>
                                <label> Rewind </label>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" ></script>
<script src="{{ asset('/js/BsMultipleSelect/BsMultiSelect.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $(".tag_table").DataTable({
            // pageLength : 20
            info : false,
            paging: false,
            searching : false
        });

        $(".company_table").DataTable({
            pageLength : 20
        });

        $(".tags").bsMultiSelect();


        $("#btn_add_tag").on("click", function(){
            $("#add_tag_modal").modal("show");
        });

        $(".edit_tag").on("click", function(){
            var value = $(this).data("value");
            $("#edit_tag_modal").find("form").prop("action", "/ctags/"+value.id);
            $("#edit_tag").val(value.tag);
            $("#preview_tag_current_unselected_image").prop("src", "/"+value.unselected_image);
            $("#preview_tag_current_selected_image").prop("src", "/"+value.selected_image);
            $("#edit_tag_modal").modal("show");
        })

        $(".delete_tag").on("click", function(){
            var confirm = window.confirm("Are you sure?");
            if(!confirm) return;
            $(this).parent().find(".delete_form").trigger("submit");
        })

        $("#btn_add_company").on("click", function(){
            $("#add_company_modal").modal("show");
        });

        $("#create_company_tags_select").on("change", function(){
            $("#create_compnay_tags").val(JSON.stringify($(this).val()));
        });

        $(".edit_company").on("click", function(){
            var value = $(this).data("value");
            $("#edit_company_form").prop("action","/packageitem/" + value.id);
            $("#edit_username").val(value.username);
            $("#edit_email").val(value.email);
            $("#edit_city").val(value.city);
            $("#edit_country").val(value.country);
            $("#edit_phone").val(value.phone);
            $("#edit_company_modal").modal("show");
        });

        $("#edit_company_tags_select").on("change", function(){
            $("#edit_company_tags").val(JSON.stringify($(this).val()));
        });

        $(".delete_company").on("click", function(){
            var confirm = window.confirm("Are you sure?");
            if(!confirm) return;

            $(this).parent().find(".delete_form").trigger("submit");
        })


        $(".company_table").on("draw.dt", function(){

            $(".delete_company").on("click", function(){
                var confirm = window.confirm("Are you sure?");
                if(!confirm) return;
                $(this).parent().find(".delete_form").trigger("submit");
            });


            $(".edit_company").on("click", function(){
                var value = $(this).data("value");
                $("#edit_company_form").prop("action","/companies/" + value.id);
                $("#edit_company_name").val(value.name);
                $("#preview_company_logo").prop("src","/"+value.photo);
                $("#edit_company_website").val(value.website);
                $("#edit_company_desc").val(value.desc);
                var tags = JSON.parse(value.tags);
                $("#edit_company_tags_select").val(tags);
                $("#edit_company_tags").val(value.tags);
                $('#edit_company_tags_select').bsMultiSelect("UpdateData");
                $("#edit_company_modal").modal("show");
            });

        })
    })
</script>
