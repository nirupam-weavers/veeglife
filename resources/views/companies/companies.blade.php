@extends('layouts.app')
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="row">
            <div class="col-sm-8">
                <div class="main-card card mb-3">
                    <div class="card-header w-100 justify-content-between">
                        <h4 class="card-title">Companies</h4>
                        <button class="btn btn-primary btn-sm" id="btn_add_company"><i class="fa fa-plus"></i> Add Company </button>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="mb-0 table table-striped company_table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Logo</th>
                                    <th>Title</th>
                                    <th>Website</th>
                                    <th>Description</th>
                                    <th>Tags</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($companies as $key => $company)
                                <tr>
                                    <td> {{ $key + 1 }} </td>
                                    <td><img src="{{ asset($company['photo'])}}" style="width:40px; height:40px"> </td>
                                    <td>{{ $company['name'] }}</td>
                                    <td><a target="_blank" href="{{ $company['website']}}">Go to Website</a></td>
                                    <td>{{ $company['desc'] }}</td>
                                    <td>
                                        <?php
                                            $tag_ids = json_decode($company['tags']);
                                            $selected_tags = [];
                                            foreach($tag_ids as $tid){
                                                foreach($tags as $tag){
                                                    if($tag['id'] == $tid) array_push($selected_tags, $tag['tag']);
                                                }
                                            }
                                            echo implode(", ", $selected_tags);
                                        ?>
                                    </td>
                                    <td style="width : 170px">
                                        <button class="btn btn-primary btn-sm edit_company" data-value='{{json_encode($company)}}'>Edit</button>
                                        <button class="btn btn-danger btn-sm delete_company">Delete</button>
                                        <a class="btn btn-primary btn-sm edit_company_images" href="{{ route('companies.images', ['id'=> $company['id'] ]) }}">Images</a>
                                        <form method="POST" action="{{ route('companies.destroy', [ 'company'=>$company['id'] ] ) }}" class="delete_form">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="main-card card mb-3">
                    <div class="card-header w-100 justify-content-between">
                        <h4 class="card-title">Tag Management</h4>
                        <button class="btn btn-primary btn-sm" id="btn_add_tag" ><i class="fa fa-plus"></i> Add
                            Tag</button>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped mb-0 tag_table ">
                            <thead>
                                <th>#</th>
                                <th>Tag</th>
                                <th>Image</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @foreach($tags as $key=>$tag)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $tag['tag'] }}</td>
                                    <td><img src="{{asset($tag['selected_image'])}}" style="width : 40px; height:40px"/></td>
                                    <td>
                                        <button class="btn btn-primary btn-sm edit_tag" data-value='{{json_encode($tag)}}'>Edit</button>
                                        <button class="btn btn-danger btn-sm delete_tag">Delete</button>
                                        <form method="POST" action="{{ route('ctags.destroy', [ 'ctag'=>$tag['id'] ] ) }}" class="delete_form">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<div class="modal fade" id="add_tag_modal" tabindex="-1" role="dialog" aria-labelledby="addTagModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{route('ctags.store')}}" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="addTagModalLabel">New Tag</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label class="control-label">Tag Name</label>
                            <input type="text" name="tag" class="form-control" required >
                        </div>
                        <div class="col-sm-12 form-group">
                            <label class="control-label">Selected Image</label>
                            <input type="file" name="selected_image" class="form-control" accept="image/*" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>    
        </div>
    </div>
</div>

<div class="modal fade" id="edit_tag_modal" tabindex="-1" role="dialog" aria-labelledby="addTagModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-header">
                    <h5 class="modal-title" id="addTagModalLabel">Edit Tag</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label class="control-label">Tag Name</label>
                            <input type="text" name="tag" class="form-control" required id="edit_tag">
                        </div>
                        <div class="col-sm-12 form-group">
                            <img id="preview_tag_current_selected_image" style="width:40px; height : 40px">
                            <label class="control-label">Selected Image</label>
                            <input type="file" name="selected_image" class="form-control" accept="image/*" id="edit_tag_selected_image">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                </div>
            </form>    
        </div>
    </div>
</div>

<div class="modal fade" id="add_company_modal" tabindex="-1" role="dialog" aria-labelledby="addTagModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{route('companies.store')}}" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="addTagModalLabel">New Tag</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label class="control-label">Company Name *</label>
                            <input type="text" name="name" class="form-control" required >
                        </div>
                        <div class="col-sm-12 form-group">
                            <label class="control-label">Company Logo *</label>
                            <input type="file" name="photo" class="form-control" accept="image/*" required>
                        </div>
                        <div class="col-sm-12 form-group">
                            <label class="control-label">Website *</label>
                            <input type="url" name="website" class="form-control" required>
                        </div>
                        <div class="col-sm-12 form-group">
                            <label class="control-label">Description *</label>
                            <textarea class="form-control" name="desc" required></textarea>
                        </div>
                        <div class="col-sm-12 form-group">
                            <label class="control-label">Tags</label>
                            <select class="w-100 tags" class="form-control"  multiple="multiple" id="create_company_tags_select" >
                                @foreach($tags as $tag)
                                <option value="{{$tag['id']}}">{{$tag['tag']}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="tags"  id="create_compnay_tags" required />
                        </div>
                        <div class="col-sm-12 form-group">
                            <label class="control-label">Photos</label>
                            <input type="file" name="images[]" multiple class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>    
        </div>
    </div>
</div>

<div class="modal fade" id="edit_company_modal" tabindex="-1" role="dialog" aria-labelledby="addTagModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="" enctype="multipart/form-data" id="edit_company_form">
                @csrf
                @method('PUT')
                <div class="modal-header">
                    <h5 class="modal-title" id="addTagModalLabel">New Tag</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label class="control-label">Company Name *</label>
                                <input type="text" name="name" class="form-control" required id="edit_company_name" >
                            </div>
                            <div class="col-sm-12 form-group">
                                <img id="preview_company_logo" style="width : 40px; height:40px"/>
                                <label class="control-label">Company Logo</label>
                                <input type="file" name="photo" class="form-control" accept="image/*"  id="edit_company_photo">
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="control-label">Website *</label>
                                <input type="url" name="website" class="form-control" required id="edit_company_website">
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="control-label">Description *</label>
                                <textarea class="form-control" name="desc" required id="edit_company_desc"></textarea>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="control-label">Tags</label>
                                <select class="w-100 tags" class="form-control"  multiple="multiple" id="edit_company_tags_select" >
                                    @foreach($tags as $tag)
                                    <option value="{{$tag['id']}}">{{$tag['tag']}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="tags"  id="edit_company_tags" required />
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>    
        </div>
    </div>
</div>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" ></script>
<script src="{{ asset('/js/BsMultipleSelect/BsMultiSelect.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $(".tag_table").DataTable({
            // pageLength : 20
            info : false,
            paging: false,
            searching : false
        });

        $(".company_table").DataTable({
            pageLength : 20
        });

        $(".tags").bsMultiSelect();


        $("#btn_add_tag").on("click", function(){
            $("#add_tag_modal").modal("show");
        });

        $(".edit_tag").on("click", function(){
            var value = $(this).data("value");
            $("#edit_tag_modal").find("form").prop("action", "/ctags/"+value.id);
            $("#edit_tag").val(value.tag);
            $("#preview_tag_current_unselected_image").prop("src", "/"+value.unselected_image);
            $("#preview_tag_current_selected_image").prop("src", "/"+value.selected_image);
            $("#edit_tag_modal").modal("show");
        })

        $(".delete_tag").on("click", function(){
            var confirm = window.confirm("Are you sure?");
            if(!confirm) return;
            $(this).parent().find(".delete_form").trigger("submit");
        })

        $("#btn_add_company").on("click", function(){
            $("#add_company_modal").modal("show");
        });

        $("#create_company_tags_select").on("change", function(){
            $("#create_compnay_tags").val(JSON.stringify($(this).val()));
        });

        $(".edit_company").on("click", function(){
            var value = $(this).data("value");
            $("#edit_company_form").prop("action","/companies/" + value.id);
            $("#edit_company_name").val(value.name);
            $("#preview_company_logo").prop("src","/"+value.photo);
            $("#edit_company_website").val(value.website);
            $("#edit_company_desc").val(value.desc);
            var tags = JSON.parse(value.tags);
            $("#edit_company_tags_select").val(tags);
            $("#edit_company_tags").val(value.tags);
            $('#edit_company_tags_select').bsMultiSelect("UpdateData");
            $("#edit_company_modal").modal("show");
        });

        $("#edit_company_tags_select").on("change", function(){
            $("#edit_company_tags").val(JSON.stringify($(this).val()));
        });

        $(".delete_company").on("click", function(){
            var confirm = window.confirm("Are you sure?");
            if(!confirm) return;

            $(this).parent().find(".delete_form").trigger("submit");
        })

        
        $(".company_table").on("draw.dt", function(){

            $(".delete_company").on("click", function(){
                var confirm = window.confirm("Are you sure?");
                if(!confirm) return;
                $(this).parent().find(".delete_form").trigger("submit");
            });


            $(".edit_company").on("click", function(){
                var value = $(this).data("value");
                $("#edit_company_form").prop("action","/companies/" + value.id);
                $("#edit_company_name").val(value.name);
                $("#preview_company_logo").prop("src","/"+value.photo);
                $("#edit_company_website").val(value.website);
                $("#edit_company_desc").val(value.desc);
                var tags = JSON.parse(value.tags);
                $("#edit_company_tags_select").val(tags);
                $("#edit_company_tags").val(value.tags);
                $('#edit_company_tags_select').bsMultiSelect("UpdateData");
                $("#edit_company_modal").modal("show");
            });

        })
    })
</script>