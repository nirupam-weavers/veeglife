@extends('layouts.app')
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-card card mb-3">
                    <div class="card-header d-flex justify-content-between">
                        <h4 class="card-title">{{ $company['name']}}</h4>
                        <button class="btn btn-primary btn-sm" id="btn_add_new_image">Add New Image</button>
                        <form class="d-none" method="POST" action="{{ route('companies.image', ['id'=>$company['id']]) }}" id="new_form" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <input type="file" name="image" id="new_image_input" accept="image/*">
                        </form>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped mb-3">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Preview</th>
                                    <th>Path</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $images = json_decode($company['images']); ?>
                                @if($images)
                                @foreach($images as $key=>$image)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td> <img src="{{ asset($image)}}" /></td>
                                    <td>{{ $image }}</td>
                                    <td>
                                        <button class="btn btn-danger btn-sm delete_image" data-image="{{ $image }}">Delete</button>
                                        <form method="POST" action="{{ route('companies.delete_image', ['id'=> $company['id'],'image' => base64_encode($image)])}}">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" ></script>
<script>
    $(document).ready(function(){

        $(".table").DataTable();
        $(".delete_image").on("click", function(){
            var confirm = window.confirm("Are you sure?");
            if(!confirm) return;
            $(this).parent().find("form").trigger("submit");
        })

        $("#btn_add_new_image").on("click", function(){
            $("#new_image_input").trigger("click");
        })

        $("#new_image_input").on("change", function(){
            $("#new_form").trigger("submit");
        })

    })
</script>
