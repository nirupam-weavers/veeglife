@extends('layouts.app')
@section('content')

<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="row">
            <div class="col-sm-4">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <!-- Photos Carousel -->
                        <?php $photos = json_decode($user->photos); ?>
                        <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @if($photos)
                                    @foreach($photos as $key=>$photo)
                                    <div class="carousel-item {{ $key == 0 ? 'active' : ''}}">
                                        <img class="d-block w-100 user-photo" src="{{ asset($photo) }}">
                                    </div>
                                    @endforeach
                                @else
                                    <div class="carousel-item active">
                                        <img class="d-block w-100 user-photo" src="{{ asset('images/admin/blank.png')}}">
                                    </div>
                                @endif
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls1" role="button"
                                data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls1" role="button"
                                data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <!-- User Name and Location -->
                        <div class="mt-3 justify-content-center w-100">
                            <h3 class="text-center">{{$user->firstname}} {{ $user->lastname}}</h3>
                        </div>
                        <div class="mt-3 justify-content-center w-100 d-flex">
                            @if($user->blocked == 0)
                            <form method="POST" action="{{route('block', ['user_id'=>$user->id])}}">
                                @csrf
                                <button class="btn btn-sm btn-danger btn_block_user text-uppercase"><i class="fa fa-lock"></i> &nbsp;Block</button>
                            </form>
                            @else
                            <form method="POST" action="{{ route('unblock', ['user_id'=>$user->id])}}">
                                @csrf
                                <button class="btn btn-sm btn-success text-uppercase"><i class="fa fa-unlock"></i>&nbsp;Unblock</button>
                            </form>
                            @endif
                            <a href="{{route('messages', ['username'=>$user->username])}}" class="btn btn-primary btn-sm text-uppercase ml-2"><i
                                    class="fa fa-comments"></i>&nbsp; Messages </a>
                        </div>
                        <div class="mt-3">
                            <h5 class=""><i class="pe-7s-map-marker"></i>&nbsp;&nbsp;{{$user->city}},
                                {{ $user->country}}</h5>
                            <h5 class=""><i
                                    class="pe-7s-date"></i>&nbsp;&nbsp;{{Carbon\Carbon::create($user->birthday)->isoFormat("MM/DD/YYYY").", ".Carbon\Carbon::parse($user->birthday)->age}}
                            </h5>
                            <h5 class=""><i class="pe-7s-users"></i>&nbsp;&nbsp;{{$user->identifies->identify}}</h5>
                            <?php
                                $playgrounds = [];
                                if($user->romance == 1) array_push($playgrounds, "Romance");
                                if($user->networking == 1) array_push($playgrounds, "Netwoking");
                                if($user->friends == 1) array_push($playgrounds, "Friendship");
                            ?>
                            <h5 class=""><i class="pe-7s-speaker"></i>&nbsp;&nbsp;{{implode(", ",$playgrounds)}}</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h4 class="card-title">Account</h4>
                        <div class="mt-4">
                            <h5 class="card-subtitle text-uppercase">User Information</h5>
                            <div class="m-4">
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <label class="text-uppercase">Member name</label>
                                        <input class="form-control" placeholder="Member Name"
                                            value="{{$user->username}}" disabled>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="text-uppercase">Email</label>
                                        <input class="form-control" placeholder="Email" value="{{$user->email}}"
                                            disabled>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="text-uppercase">Phone Number</label>
                                        <input class="form-control" placeholder="Phone Number" value="{{$user->phone}}"
                                            disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="divider"></div>
                        <div class="mt-4">
                            <h5 class="card-subtitle text-uppercase">Personal Information</h5>
                            <div class="m-4 w-100">
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <label class="text-uppercase">Favorite Cuisine</label>
                                        <?php
                                            $cuisines = [];
                                            foreach(json_decode($user->cuisine_id) as $cusine){
                                                array_push($cuisines, $cusine->name);
                                            }
                                        ?>
                                        <input class="form-control" placeholder="Favorite Cuisine"
                                            value="{{ implode(', ', $cuisines)}}" disabled>
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label class="text-uppercase">Industry</label>
                                        <?php
                                            $industries = [];
                                            foreach(json_decode($user->industry_id) as $industry){
                                                array_push($industries, $industry->name);
                                            }
                                        ?>
                                        <input class="form-control" placeholder="Industries"
                                            value="{{ implode(', ', $industries)}}" disabled>
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label class="text-uppercase">Astrological</label>
                                        <input class="form-control" placeholder="Unknown"
                                            value="{{ $user->astrological?$user->astrological->astrological:'' }}" disabled />
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label class="text-uppercase">Activities</label>
                                        <?php
                                            $activities = [];
                                            foreach(json_decode($user->activities) as $activity){
                                                array_push($activities, $activity->name);
                                            }
                                        ?>
                                        <input class="form-control" placeholder="Unknown"
                                            value="{{ implode(', ', $activities) }}" disabled />
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label class="text-uppercase">Marital Status</label>
                                        <input class="form-control" placeholder="Unknown"
                                            value="{{ $user->maritals?$user->maritals->name:'' }}" disabled />
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label class="text-uppercase">Spiritual Beliefs</label>
                                        <input class="form-control" placeholder="Unknown"
                                            value="{{ $user->spiritual?$user->spiritual_belifes->name:'' }}" disabled />
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label class="text-uppercase">Alcohol</label>
                                        <input class="form-control" placeholder="Unknown"
                                            value="{{ $user->alcohols?$user->alcohols->name:'' }}" disabled />
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label class="text-uppercase">Tobacco</label>
                                        <input class="form-control" placeholder="Unknown"
                                            value="{{ $user->tobaccos?$user->tobaccos->name:'' }}" disabled />
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label class="text-uppercase">420</label>
                                        <input class="form-control" placeholder="Unknown"
                                            value="{{ $user->four20?$user->four20->name:'' }}" disabled />
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label class="text-uppercase">Have Children?</label>
                                        <input class="form-control" placeholder="Unknown"
                                            value="{{ $user->have_children?$user->have_children->name:'' }}" disabled />
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label class="text-uppercase">Have Any Animal Friends?</label>
                                        <input class="form-control" placeholder="Unknown"
                                            value="{{ $user->have_animal?$user->have_animal->name:'' }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="divider"></div>
                        <div class="mt-4">
                            <h5 class="card-subtitle text-uppercase">Bio</h5>
                            <div class="m-4 w-100">
                                <div class="row">
                                    @foreach($user->params as $param)
                                    <div class="col-sm-12 form-group">
                                        <label class="text-uppercase">{{ $param->playground }}</label>
                                        <p class="card-subtitle pl-2 pr-2 pt-2">{{ $param->bio }}</p>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

