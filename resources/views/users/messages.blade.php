@extends('layouts.app')
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Messages</h5>
                        <table class="mb-0 table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Playground</th>
                                    <th>From/To</th>
                                    <th>Content</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($messages as $key=>$message)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ Carbon\Carbon::create($message['timestamp'])->isoFormat("MM/DD/YYYY H:m:s") }}</td>
                                    <td>{{ $message['user']['_id'] == $user['id']? "Send":"Receive"}}</td>
                                    <td>{{ ucfirst($message['playground'])}}</td>
                                    <td>{{ $message['user']['_id'] == $user['id']?$message['receiver']['firstname']." ".$message['receiver']['lastname']: $message['user']['firstname']." ". $message['user']['lastname'] }} </td>
                                    <td>
                                        @if($message['image'] == "")
                                            {{ $message['text'] }}
                                        @else
                                            <img src="{{ $message['image'] }}" style="width : 100px" >
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" ></script>

<script>

    $(document).ready(function(){
        $(".table").DataTable({
            pageLength : 20
        });
    });
</script>