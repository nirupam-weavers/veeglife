@extends('layouts.app')
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Users</h5>
                        <table class="mb-0 table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Phone</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $key=>$user)
                                <tr>
                                    <th scope="row">{{$key + 1}}</th>
                                    <td>{{$user->firstname}}</td>
                                    <td>{{$user->lastname}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{$user->username}}</td>
                                    <td>{{$user->email}}</td>
                                    <td style="width : 280px">
                                        <a class="btn-primary btn btn-sm profile" data-id="{{$user->id}}" href="{{route('user', ['username'=>$user->username])}}"><i class="fa fa-user"></i> &nbsp;Profile </a>
                                        <a class="btn-success btn btn-sm messages" href="{{ route('messages', ['username'=>$user->username])}}"><i class="fa fa-comment"></i> &nbsp;Messages</a>
                                        <a class="btn-danger btn btn-sm delete"> <i class="fa fa-trash"></i> &nbsp;Delete </a>
                                        <form method="POST" action="{{ route('remove_user', ['username'=>$user->username])}}" class="remove_form">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" ></script>

<script>

    $(document).ready(function(){

        $(".table").DataTable({
            pageLength : 20
        });

        $(".delete").on("click", function(){
            var id = $(this).data("id");
            var confirm = window.confirm("Are you sure?");
            if(!confirm) return;
            $(this).parent().find(".remove_form").trigger("submit");
        })

        $(".table").on("draw.dt", function(){
            $(".delete").unbind("click").on("click", function(){
                var id = $(this).data("id");
                var confirm = window.confirm("Are you sure?");
                if(!confirm) return;
                $(this).parent().find(".remove_form").trigger("submit");
            })

        })
    })
</script>
