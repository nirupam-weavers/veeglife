<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use RealRashid\SweetAlert\Facades\Alert;


class PackageItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $userscount = User::all()->count();
        $photocount = User::sum('photo');
        $videocount = User::sum('video');
        $filtercount = User::sum('filter');
        $tossedcount = User::sum('tossed');
        $rewindcount = User::sum('rewind');

        return view("Packages.packageitems", [ 'packages'=>$users,'userscount'=>$userscount,'photocount'=>$photocount,
            'videocount'=>$videocount,'filtercount'=>$filtercount,'tossedcount'=>$tossedcount,'rewindcount'=>$rewindcount,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = array();
        $data['photo']  = $request->photo;
        $data['video']  = $request->video;
        $data['filter'] = $request->filter;
        $data['tossed'] = $request->tossed;
        $data['rewind'] = $request->rewind;

        if(!$data['photo']){
            $data['photo']  = 0;
        }

        if(!$data['video']){
            $data['video']  = 0;
        }

        if(!$data['filter']){
            $data['filter']  = 0;
        }

        if(!$data['tossed']){
            $data['tossed']  = 0;
        }

        if(!$data['rewind']){
            $data['rewind']  = 0;
        }

        DB::table('users')->update($data);
        return back()->withToastSuccess('Settings Created Successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array();
        $data['photo']  = $request->photo;
        $data['video']  = $request->video;
        $data['filter'] = $request->filter;
        $data['tossed'] = $request->tossed;
        $data['rewind'] = $request->rewind;

        if(!$data['photo']){
            $data['photo']  = 0;
        }

        if(!$data['video']){
            $data['video']  = 0;
        }

        if(!$data['filter']){
            $data['filter']  = 0;
        }

        if(!$data['tossed']){
            $data['tossed']  = 0;
        }

        if(!$data['rewind']){
            $data['rewind']  = 0;
        }

        User::find($id)->update($data);
        return back()->withToastSuccess('Settings Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return back();
    }
}
