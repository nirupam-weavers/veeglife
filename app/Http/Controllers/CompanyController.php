<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\CompanyTag;
use App\Traits\ImageUploader;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use ImageUploader;

    public function __construct()
    {
        $this->middleware('auth');
    }
    // 
    public function index()
    {
        $companies = Company::all();
        $tags = CompanyTag::all();
        return view("companies.companies", [ 'companies'=>$companies, 'tags'=>$tags ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // die(print_r($request->toArray()));
        $data = $request->validate([
            'name' => "string|required",
            'photo' => "image|required",
            'website'=> "url|required",
            "desc" => "string|required",
            'tags'=> "string|required",
            "images"=>"array|nullable"
        ]);
        $data['photo'] = $this->ImageUpload($data['photo']);
        if(isset($data['images'])){
            $images = [];   
            foreach($data['images'] as $image){
                $upload_url = $this->ImageUpload($image);
                array_push($images, $upload_url);
                sleep(1);
            }
            $data['images'] = json_encode($images);
        }
        Company::create($data);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         //
        $data = $request->validate([
            'name' => "string|required",
            'photo' => "image|nullable",
            'website'=> "url|required",
            "desc" => "string|required",
            'tags'=> "string|required"
        ]);
        if(isset($data['photo'])){
            $data['photo'] = $this->ImageUpload($data['photo']);
        }

        Company::find($id)->update($data);

        return back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Company::find($id)->delete();

        return back();
    }

    public function get_company_images($id){

        $company = Company::find($id);
        return view("companies.images", ['company' => $company]);
    }

    public function delete_company_image($id, $image){

        $image = base64_decode($image);
        $company = Company::find($id);
        $images = json_decode($company->images);

        foreach($images as $key=>$img){
            if($image == $img) unset($images[$key]);
        }

        $company->update(['images'=>json_encode($images)]);

        return back();


    }

    public function add_new_image($id, Request $request){

        $data = $request->validate([
            'image'=>'image|required'
        ]);
        $image = $this->ImageUpload($data['image']);
        $company = Company::find($id);
        $images = [];
        if($company->images) $images = json_decode($company->images);
        array_push($images, $image);
        $company->update(['images'=>json_encode($images)]);

        return back();
    }
}
