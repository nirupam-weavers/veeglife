<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyTag;
use App\Traits\ImageUploader;


class CompanyTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use ImageUploader;

    public function __construct()
    {
        $this->middleware('auth');
    }
    // 

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'tag'=>'required|string|unique:companytags',
            'selected_image' => 'required|image',
        ]);
        $validatedData['selected_image'] = $this->ImageUpload($validatedData['selected_image']);
        CompanyTag::create($validatedData);
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedData = $request->validate([
            "tag"=>"required|string",
            "selected_image" => "image|nullable"
        ]);
        if(isset($validatedData['selected_image'])) 
            $validatedData['selected_image'] = $this->ImageUpload($validatedData['selected_image']);

        CompanyTag::find($id)->update($validatedData);

        return back();



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        CompanyTag::find($id)->delete();

        return back();

    }

    public function get_all_tags(){
        $tags = CompanyTag::all();
        return response()->json($tags, 200);
    }
}
