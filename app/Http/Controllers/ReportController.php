<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Report;

class ReportController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $reports = Report::with("user")->with("reporter")->orderBy("id", "desc")->get();
        return view("reports.reports", ['reports'=>$reports]);
    }
}
