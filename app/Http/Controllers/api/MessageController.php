<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\api\BaseController as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Validator;
use App\MatchedList;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use App\VideoCallSchedule;
use Illuminate\Support\Facades\Http;

class MessageController extends BaseController
{
    public function get_new_connections(Request $request){
        // SELECT * FROM `matched_list` as m Join matched_list as n on m.user_id = n.tossed_id and m.tossed_id = n.user_id and m.playground = n.playground where m.user_id = 3
        $user_id = $request->user_id;
        $playground = $request->playground;
        $users = DB::select("SELECT u.* FROM (SELECT a.* FROM `matched_list` as a JOIN `matched_list` as b ON a.user_id = b.tossed_id and a.tossed_id = b.user_id and a.playground = b.playground WHERE a.user_id = ? AND  a.playground = ?) as m JOIN users as u on m.tossed_id = u.id WHERE m.is_new = 1",
            [$user_id, $playground ]
        );
        return response()->json(["success"=>"success", "data"=>$users], 200);
    }

    public function get_twilio_access_token(Request $request){
        try{
            $account_sid = env("TWILIO_ACCOUNT_SID", "");
            $api_sid = env("TWILIO_API_SID", "");
            $api_serect = env("TWILIO_API_SECRET", "");
            $identify = $request->identify;
            $token = new AccessToken(
                $account_sid,
                $api_sid,
                $api_serect,
                3600,
                $identify
            );
            $room = $request->ruid;
            // Create Video grant
            $videoGrant = new VideoGrant();
            $videoGrant->setRoom($room);
            // Add grant to token
            $token->addGrant($videoGrant);
            // render token to string
            // die( $token->toJWT());
            return response()->json(["success"=>"success", "data"=>$token->toJWT()], 200);
        }
        catch(Exception $e){
            return response()->json(["error"=>"error"], 500);
        }

    }

    public function set_video_call_schedule(Request $request){
        // data
        /*
        caller, receiver, date, time, accepted, create_at, completed
        */
        $caller = $request->caller;
        $receiver = $request->receiver;
        $date = $request->mdate;
        $time = $request->mtime;

        $result = VideoCallSchedule::create(['caller'=>$caller, "receiver"=>$receiver, "mdate"=>$date, "mtime"=>$time]);
        return response()->json(["data"=>$result], 200);
    }

    // For Video Call Schedule

    public function accpept_video_call_schedule(Request $request){

        $id = $request->tid;
        VideoCallSchedule::find($id)->update(["accepted" => 1]);

        return response()->json(["success"=>"success"]);

    }

    public function suggest_video_call_with_new_time(Request $request){

        $id = $request->tid;
        VideoCallSchedule::find($id)->update(["completed"=>1]);
        // Set New Schedule

        return response()->json(["success"=>"success"]);
    }

    public function decline_video_call_schedule(Request $request){
        $id = $request->tid;
        VideoCallSchedule::find($id)->update(["completed"=>1]);
        return response()->json(["success"=>"success"]);
    }

    public function video_chat_schedule_notification(){
        $schedules = VideoCallSchedule::with("caller")->with("receiver")->where(["accepted" => 1, "completed"=> 0])->get();

        $now = date_create(date("Y-m-d H:i:s"));

        foreach($schedules as $key=>$schedule){
            $schedule = $schedule->toArray();
            $date = date_create($schedule['mdate'].$schedule['mtime']);
            $interval = date_diff($now, $date);
            $diff = intval((($interval->y*365.25 + $interval->m * 30 + $interval->d) * 24 + $interval->h) * 60 + $interval->i + $interval->s/60);
            if($interval->invert) $diff = -1 * $diff;

            $deviceToken1 = $schedule['caller']['push_tokens'];
            $deviceToken2 = $schedule['receiver']['push_tokens'];
            $type = "reminder";
            $title="Video Call Schedule";

            if($diff == 5){
                $body = "You will have a video call after 5 minutes.";
                $response = Http::post(config('app.cloud_url').'sendPushNotification', [
                    'deviceToken' => [$deviceToken1, $deviceToken2],
                    'type' => $type,
                    "title" => $title,
                    "body" => $body
                ]);
            }
            else if($diff == 60){
                $body = "You will have a video call after 1 hour!";
                $response = Http::post(config('app.cloud_url').'sendPushNotification', [
                    'deviceToken' => [$deviceToken1, $deviceToken2],
                    'type' => $type,
                    "title" => $title,
                    "body" => $body
                ]);
            }
            else if($diff == 1440 ){
                $body = "You will have a video call after 1 day.";
                $response = Http::post(config('app.cloud_url').'sendPushNotification', [
                    'deviceToken' => [$deviceToken1, $deviceToken2],
                    'type' => $type,
                    "title" => $title,
                    "body" => $body
                ]);
            }

        }
        return response()->json(["schedules"=>$schedules,"DIFF"=>$diff,"success"=>"success"]);
    }


}
