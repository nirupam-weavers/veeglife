<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\api\BaseController as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Square\Models\Money;
use Square\Models\CreatePaymentRequest;
use Square\Exceptions\ApiException;
use Square\SquareClient;
use App\PaymentHistory;
use Carbon\Carbon;

class PaymentController extends BaseController{
    public $successStatus = 200;

    public function create_payment($user_id, $nonce,  $amount){
        $environment = "sandbox";
        $accessToken = "EAAAEHio53fSG_VWe0vvNjdxydNGG1xLzal6yR60pmJ-41eGzp-Ti_uDan1XXgy0";
        $client = new SquareClient([
            'accessToken' => $accessToken,
            'environment' => $environment
        ]);
        $nonce = $nonce;
        $user_id = $user_id;
        $payment_api = $client->getPaymentsApi();
        $money = new Money();
        $money->setAmount($amount);
        $money->setCurrency("USD");
        $create_payment_request = new CreatePaymentRequest($nonce, uniqid(), $money);
        try{
            $response = $payment_api->createPayment($create_payment_request);
            if($response->isError()){
                $errors = $response->getErrors();
                return array("success"=>false, "error"=>$errors);
            }
            PaymentHistory::create(['user_id'=>$user_id, 'amount'=>$amount]);
            return array("success"=>true);
        }catch(ApiException $e){
            return array("success"=>false, "error"=>$e->getResponseBody());
        }
    }


    public function purchase_item(Request $request){
        $user_id = $request->user_id;
        $nonce = $request->nonce;
        $amount = $request->price;
        $item = $request->item;
        $itemArray = explode(',', $item);
        $payment = $this->create_payment($user_id, $nonce, $amount);
        if($payment['success']){
            $now = Carbon::now();

            if (in_array("tossed", $itemArray))
            {
                $expire_at = $now->addMonth();
                $t = User::where(['id'=>$user_id])->update(["tossed"=>1, "tossed_expire_at"=>$expire_at]);
            }
            if(in_array("rewind", $itemArray))
            {
                $expire_at = $now->addMonth();
                $t = User::where(['id'=>$user_id])->update(["rewind"=>1, "rewind_expire_at"=>$expire_at]);
            }
            if(in_array("photo", $itemArray))
            {
                $expire_at = $now->addMonth();
                $t = User::where(['id'=>$user_id])->update(["photo"=>1, "photo_expire_at"=>$expire_at]);
            }
            if(in_array("video", $itemArray))
            {
                $expire_at = $now->addMonth();
                $t = User::where(['id'=>$user_id])->update(["video"=>1, "video_expire_at"=>$expire_at]);
            }
            if(in_array("filter", $itemArray))
            {
                $expire_at = $now->addMonth();
                $t = User::where(['id'=>$user_id])->update(["filter"=>1, "filter_expire_at"=>$expire_at]);
            }
            if(in_array("1month", $itemArray))
            {
                $expire_at = $now->addMonth();
                $t = User::where(['id'=>$user_id])
                    ->update([  "video"=>1,
                        "video_expire_at"=>$expire_at,
                        "tossed"=>1,
                        "tossed_expire_at"=>$expire_at,
                        "rewind"=>1,
                        "rewind_expire_at"=>$expire_at,
                        "filter"=>1,
                        "filter_expire_at"=>$expire_at
                    ]);
            }
            if(in_array("3months", $itemArray))
            {
                $expire_at = $now->addMonth(3);
                $t = User::where(['id'=>$user_id])
                    ->update([  "video"=>1,
                        "video_expire_at"=>$expire_at,
                        "tossed"=>1,
                        "tossed_expire_at"=>$expire_at,
                        "rewind"=>1,
                        "rewind_expire_at"=>$expire_at,
                        "filter"=>1,
                        "filter_expire_at"=>$expire_at
                    ]);
            }
            if(in_array("6months", $itemArray))
            {
                $expire_at = $now->addMonth(6);
                $t = User::where(['id'=>$user_id])
                    ->update([  "video"=>1,
                        "video_expire_at"=>$expire_at,
                        "tossed"=>1,
                        "tossed_expire_at"=>$expire_at,
                        "rewind"=>1,
                        "rewind_expire_at"=>$expire_at,
                        "filter"=>1,
                        "filter_expire_at"=>$expire_at
                    ]);
            }
            $user = User::with("params")->find($user_id);
            return response()->json(['user' => $user], $this->successStatus);
        }else{
            return response()->json(['error' => $payment['error']], $this->successStatus);
        }

    }
}
?>
