<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\api\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Company;
use App\CompanyTag;

class CompanyController extends BaseController
{
    //
    public function get_all_tags(){

        $tags = CompanyTag::all();

        return response()->json($tags, 200);

    }

    public function get_companies(){

        $companies = Company::all();

        return response()->json($companies, 200);

    }
}
