<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\api\BaseController as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Validator;
use App\MatchedList;
use App\Param;
use Illuminate\Support\Facades\Http;

class MatchController extends BaseController
{
    //
    public $successStatus = 200;

    public function get_matches(Request $request){

        $user_id = $request->user_id;
        $playground = $request->playground;

        $user = User::find($user_id);
        if(!$user) return $this->sendError("Not Found User!");

        $param = Param::where(["user_id"=>$user_id, "playground"=>$playground])->first();

        if($param == NULL){
            return response()->json(['count'=>0, "matches"=>[]],$this->successStatus);
        }
        $age_range = json_decode($param->ages);
        $finding_identifies = $param->gender_industry;
        $current_year = date("Y");
        // For Search Age
        $max_born_year = date("Y", strtotime($current_year." -".$age_range[0]." years"));
        $min_born_year = date("Y", strtotime($current_year." -".$age_range[1]." years"));
        // Get Matches

        $sql = "SELECT users.* FROM users
                LEFT JOIN (SELECT * FROM matched_list WHERE user_id = $user_id AND playground = '$playground' ) as m ON m.tossed_id = users.id
                    WHERE m.user_id IS NULL
                    AND Year(birthday) >= $min_born_year
                    AND Year(birthday) <= $max_born_year
                    AND $playground = 1
                    AND users.id != $user_id";

        $users = DB::select($sql);

        $my_position = json_decode($user->position);
        $matches = [];
        // Get distance between User and Matches.
        $search_distance = $param->distance;
        foreach($users as $key=>$one){
            $p1 = $my_position;
            $p2 = json_decode($one->position);
            // $R = 2460; // Earth’s mean radius in mile
		  	// $dLat = $p2->lat - $p1->lat;
		  	// $dLong = $p2->lng - $p1->lng;
            // $a = pow(cos($p2->lat) * sin($dLong), 2) + pow(cos($p1->lat) * sin($p2->lat) - sin($p1->lat) * cos($p2->lat) * cos($dLong), 2);
            // $b = sin($p1->lat) * sin($p2->lat) + cos($p1->lat) * cos($p2->lat) * cos($dLong);
            // $angle = atan2(sqrt($a), $b);
            // $d = intval($R * $angle);
            $latitude1 = $p1->lat;
            $latitude2 = $p2->lat;
            $theta = $p1->lng - $p2->lng;
            $distance = (sin(deg2rad($latitude1))*sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1))*cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
            $distance = acos($distance);
            $distance = rad2deg($distance);
            $distance = $distance * 60 * 1.1515;
            if($distance <= $search_distance){
                $one->dis = intval($distance);
                $matches[] = $one;
            }
        }
        return response()->json(['count'=>count($matches), "matches"=>$matches],$this->successStatus);
    }

    public function pass_match(Request $request){
        $validator = Validator::make($request->all(), [
            "user_id"=> 'required|integer',
            "match_id"=> 'required|integer',
            "playground" => 'required|string|in:romance,networking,friends'
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);
        }
        $validated = $validator->validated();
        $data = [
            "user_id"=>$validated['user_id'],
            "tossed_id"=>$validated['match_id'],
            "playground" => $validated['playground']
        ];

        $count = MatchedList::where($data)->count();
        if($count > 0){
            return $this->sendResponse(["success"=>"fail"],"Duplicate Record");
        }

        $data['toss_type'] = 0;
        $matched = MatchedList::create($data);

        if($matched) return $this->sendResponse(["success"=>"success"], "Insert Database Success");
        else return $this->sendError("Insert Database error",[], 500);
    }

    public function toss_match(Request $request){
        $validator = Validator::make($request->all(), [
            "user_id"=> 'required|integer',
            "match_id"=> 'required|integer',
            "playground" => 'required|string|in:romance,networking,friends'
        ]);
        if ($validator->fails()) { return response()->json(['error'=>$validator->errors()], 200);}
        $validated = $validator->validated();
        $data = [
            "user_id"=>$validated['user_id'],
            "tossed_id"=>$validated['match_id'],
            "playground" => $validated['playground']
        ];
        $count = MatchedList::where($data)->count();
        if($count > 0){ return $this->sendResponse(["success"=>"fail"],"Duplicate Record"); }
        $data['toss_type'] = 1;
        $inserted = MatchedList::create($data);
        if($inserted){
            $matched = MatchedList::where(["user_id" => $validated['match_id'], "tossed_id"=>$validated['user_id'], "playground"=>$validated['playground']])->get();
            if(count($matched) > 0 ) { //Matched
                // Get Matched User's Push Token and send push notification.

                $matcher = User::find($validated['match_id']);
                $deviceToken = $matcher->push_tokens;
                $type = "match";
                $title="New Connection!";
                $body = "You have a new connection!";
                $response = Http::post('https://us-central1-veganmeet.cloudfunctions.net/sendPushNotification', [
                    'deviceToken' => $deviceToken,
                    'type' => $type,
                    "title" => $title,
                    "body" => $body
                ]);

                return $this->sendResponse(["success"=>"matched"], "Matched");
            }
            else return $this->sendResponse(["success"=>"success"], "Insert Database Success");
        }
        else return $this->sendError("Insert Database error",[], 500);
    }

    public function back_match(Request $request){

        $user_id = $request->user_id;
        $tossed_id = $request->tossed_id;
        $playground = $request->playground;
        $user = User::find($user_id);
        if($user->free_rewind > 0 && $user->rewind == 0){
            User::where(["id"=>$user_id])->update(["free_rewind" => $user->free_rewind - 1]);
        }
        MatchedList::where(["user_id"=>$user_id,"tossed_id"=>$tossed_id, "playground"=>$playground])->delete();
        return $this->sendResponse(["success"=>"success"], "");
    }

    public function update_is_new(Request $request){
        // param user1, user2, playground
        $user1 = $request->user1;
        $user2 = $request->user2;
        $playground = $request->playground;
        // $result = MatchedList::where(['user_id'=>$user1, "tossed_id"=>$user2, 'playground'=>$playground])
        //                 ->orWhere(['user_id'=>$user2, "tossed_id"=>$user1, 'playground'=>$playground])
        //                 ->update(["is_new"=>0]);

        $sql = "";
        $result = DB::select(
            "UPDATE matched_list SET is_new=0 WHERE (user_id=? AND tossed_id=? AND playground=?) OR (user_id=? AND tossed_id=? AND playground=?)",
                [$user1, $user2, $playground, $user2, $user1, $playground]);
        return response()->json(['success'=>"success"], $this->successStatus);
    }

    public function search(Request $request){

        $keyword = $request->keyword;
        $result = User::where("username", "like", "%".$keyword."%")->get();
        if($result) return response()->json($result, $this->successStatus);
        return response()->json([], $this->successStatus);
    }

    public function get_likes($user_id, $playground){

        $query = MatchedList::with("tossedUser")->where(["tossed_id"=>$user_id, "playground"=>$playground])->get();
        $likes = [];
        foreach($query as $like){
            $t = MatchedList::where(["user_id"=>$user_id, "tossed_id"=>$like->user_id, "playground"=>$playground])->get();
            if(count($t) == 0){ array_push($likes, $like); }
        }
        return $this->sendResponse(['likes'=>$likes], $this->successStatus);
    }
}
