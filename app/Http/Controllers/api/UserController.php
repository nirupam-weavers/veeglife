<?php
namespace App\Http\Controllers\api;
use Illuminate\Http\Request;
use App\Http\Controllers\api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Http;


use App\Traits\ImageUploader;
use App\User;
use App\Identify;
use App\Cuisine;
use App\Astrological;
use App\Industry;
use App\Marital;
use App\Spiritual;
use App\Alcohol;
use App\Tabacco;
use App\Friend420;
use App\Havechildren;
use App\Haveanimal;
use App\Condition;
use App\Activity;
use App\Param;
use App\LookingFor;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;



class UserController extends BaseController
{

    use ImageUploader;

    public $successStatus = 200;
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request ){
        $validator = Validator::make($request->all(), [
            "phone"=>"string|required",
            "push_tokens"=>"string"
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 400);
        }
        $input = $validator->validated();
        $user = User::with("params")->where(['phone'=>$input['phone']])->first();
        if($user && $user->count() > 0) {
            if(!empty($input['push_tokens'])){
                User::where(['phone'=>$input['phone']])->update(['push_tokens'=>$input['push_tokens']]);
            }
            return response()->json(['user'=>$user], $this->successStatus);
        }
        else return response()->json(['error'=>"Not Registered!"], 401);
    }

    /**
     * refresh fcm api
     *
     * @return \Illuminate\Http\Response
     */
    public function refresh_fcm(Request $request ){
        $user_id = $request->user_id;
        $user = User::find($user_id);
        if (!$user) {
            return response()->json(['error'=>"Not Registered!"], 401);
        }
        $validator = Validator::make($request->all(), [
            "push_tokens"=>"string"
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 400);
        }
        $input = $validator->validated();
        $user = User::with("params")->where(['id'=>$user_id])->first();
        if($user->count() > 0) {
            if(!empty($input['push_tokens'])){
                User::where(['id'=>$user_id])->update(['push_tokens'=>$input['push_tokens']]);
            }
            return response()->json(['user'=>$user], $this->successStatus);
        }
        else return response()->json(['error'=>"Not Registered!"], 401);
    }

    /**
     * remove fcm api
     *
     * @return \Illuminate\Http\Response
     */
    public function remove_fcm(Request $request ){
        $user_id = $request->user_id;
        $user = User::find($user_id);
        if (!$user) {
            return response()->json(['error'=>"Not Registered!"], 401);
        }
        $user = User::with("params")->where(['id'=>$user_id])->first();
        if($user->count() > 0) {
            $user = User::where(['id'=>$user_id])->update(['push_tokens'=>""]);
            return response()->json(['user'=>$user], $this->successStatus);
        }
        else return response()->json(['error'=>"Not Registered!"], 401);
    }

    /**
     * Check Phone Number if already existing
     */
    public function check_phonenumber(Request $request){
        $validator = Validator::make($request->all(),[
            'phone' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);
        }
        $phone = $request->phone;
        $res = User::where("phone", $phone)->count();
        if($res == 0) return response()->json(['success'=>"success"], $this->successStatus);
        else return $this->sendError("Fail!", "Oop, This phone number is already used!", 200);
    }


    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname'=>'string|required',
            'lastname' => 'string|required',
            'phone' => 'required|string|unique:users',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'birthday'=> 'required|date',
            'identify_id' => 'required|integer',
            'cuisine_id' => 'required|json',
            'astrological_id' => 'required|integer',
            'industry_id' => 'required|json',
            'password' => 'required|same:c_password',
            'position'=>'json',
            "country"=>"string",
            "city"=>"string",
            "push_tokens"=>"string"
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);
        }

        $input = $validator->validated();
        $input['password'] = bcrypt($input['password']);
        $input['push_tokens'] = $input['push_tokens'];
        $uploaded = $request->photos;
        if(isset($uploaded) && count($uploaded) > 0 ){
            $photos = [];
            foreach($uploaded as $key => $photo){
                sleep(1);
                $photos[] = $this->ImageUpload($photo);
            }
            $input['photos'] = json_encode($photos);
        }

        $user = User::create($input);
        $success['username'] =  $user->username;
        $success['user_id'] = $user->id;
        return response()->json(['success'=>$success], $this->successStatus);
    }
    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */

    public function get_general_resources() {
        $resources = array();
        try{
            $resources['identifies'] = Identify::select(["id", "identify as name"])->get();
            $resources['cuisines'] = Cuisine::select(["id", "cuisine as name"])->get();
            $resources['astrologicals'] = Astrological::select(['id', 'astrological as name'])->get();
            $resources['industries'] = Industry::select(['id', "industry as name"])->get();
            $resources['conditions'] = Condition::select(["id", "name"])->get();
            $resources['spirituals'] = Spiritual::select(["id", "name"])->get();
            $resources['maritals'] = Marital::select(["id", "name"])->get();
            $resources['alcohols'] = Alcohol::select(['id', 'name'])->get();
            $resources['tabaccoes'] = Tabacco::select(['id', "name"])->get();
            $resources['friend420s'] = Friend420::select(['id', "name"])->get();
            $resources['havechildren'] = Havechildren::select(['id', "name"])->get();
            $resources['haveanimal'] = Haveanimal::select(['id', "name"])->get();
            $resources['activities'] = Activity::select(['id','name'])->get();
            $resources['lookingfor'] = LookingFor::select(['id', 'name'])->get();
            return response()->json($resources, $this->successStatus);
        }
        catch(\Illuminate\Database\QueryException $e){
            return $this->sendError("Database Error", "Oop, Something went wrong!", 500);

        }
    }

    public function check_valid_username(Request $request){
        $validator = Validator::make($request->all(), [
            'username'=>'string|required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $username = $request->username;
        try{
            $count = User::where("username", $username)->count();
            return response()->json(array("success"=>$count),  $this->successStatus);
        }
        catch(\Illuminate\Database\QueryException $e){
            return $this->sendError("Database Error", "Oop, Something went wrong!", 500);
        }
    }

    public function check_valid_email(Request $request){
        $validator = Validator::make($request->all(),[
            "email"=>"email|required",
        ]);
        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $email = $request->email;
        try{
            $count = User::where("email", $email)->count();
            return response()->json(array("success"=>$count),  $this->successStatus);
        }
        catch(\Illuminate\Database\QueryException $e){
            return $this->sendError("Database Error", "Oop, Something went wrong!", 500);
        }
    }

    public function update_user_setting(Request $request){

        $data = $request->all();
        if(!empty($data['identify'])){
            $identify = json_decode($data['identify']);
            $array = [];
            foreach($identify as $key=>$one){ $array[] = $one->id;}
            $data['identify'] =json_encode($array);
        }
        if(!empty($data['conditions'])){
            $array = [];
            $condition = json_decode($data['conditions']);
            foreach($condition as $key=>$one){ $array[] = $one->id; }
            $data['conditions'] = json_encode($array);
        }

        $user_id = $data['user_id'];
        unset($data['user_id']);

        $result = User::where("id", $user_id)->update($data);

        $user = User::find($user_id);
        return response()->json(["user"=>$user]);

    }

    public function insert_matching_params(Request $request){

        $data['user_id'] = $request->user_id;
        $data['ages'] = $request->ages;
        $data['distance'] = $request->distance;
        $data['playground'] = $request->playground;
        $data['bio'] = $request->bio;

        $data['gender_industry'] = $request->gender_industry;
        if(!empty($data['gender_industry'])){
            $identify = json_decode($data['gender_industry']);
            $array = [];
            foreach($identify as $key=>$one){ $array[] = $one->id;}
            $data['gender_industry'] =json_encode($array);
        }
        $check = Param::where(['user_id'=>$request->user_id, "playground"=>$request->playground])->count();
        if($check){
            Param::where(['user_id'=>$request->user_id, "playground"=>$request->playground])->update($data);
            $result = Param::where(['user_id'=>$request->user_id, "playground"=>$request->playground])->first();
        }
        else {
            $result = Param::create($data);
        }

        return response()->json(['params'=>$result], 200);
    }

    function store_playgrounds(Request $request){

        $user_id = $request->user_id;
        $data['romance'] = $request->romance;
        $data['friends'] = $request->friends;
        $data['networking'] = $request->networking;

        $result = User::where('id', $user_id)->update($data);

        return response()->json(['success'=>'success']);

    }

    public function add_user_image(Request $request){
        $user_id = $request->user_id;
        $user = User::find($user_id);
        $photo = $request->photo;
        $photo = $this->ImageUpload($photo);
        $photos = json_decode( $user->photos );
        array_push($photos, $photo);
        $user->photos = $photos;
        $user->save();
        return response()->json([ "photos" => json_encode($photos) ], $this->successStatus);
    }

    public function delete_user_image(Request $request){
        $user_id = $request->user_id;
        $photos = $request->photos;
        $result = User::where("id", $user_id)->update(["photos"=>$photos]);
        return response()->json( [ "photos" => $photos ], $this->successStatus );
    }



    public function update_cuisines(Request $request){
        $user_id = $request->user_id;
        $cuisines = $request->cuisines;
        $user = User::where("id", $user_id)->update(["cuisine_id"=>$cuisines]);
        if($user > 0) return response()->json(['cuisines'=>$cuisines], $this->successStatus);
        else return response()->json(["error"=>"Can not update Cuisines."], 500);
    }

    public function update_indusries(Request $request){
        $user_id = $request->user_id;
        $industries = $request->industries;
        $user = User::where("id", $user_id)->update(["industry_id"=>$industries]);
        if($user > 0) return response()->json(['industries'=>$industries], $this->successStatus);
        else return response()->json(["error"=>"Can not update industries."], 500);
    }

    public function update_identifies(Request $request){
        $user_id = $request->user_id;
        $param_id = $request->param_id;
        $gender_industry = $request->gender_industry;
        $param = Param::where("id", $param_id)->update(["gender_industry"=>$gender_industry]);
        if($param > 0){
            $params = Param::where("user_id", $user_id)->get();
            return response()->json(['params'=>$params], $this->successStatus);
        }
        else return response()->json(["error"=>"Can not update identifies."], 500);
    }

    public function update_marital(Request $request){
        $user_id = $request->user_id;
        $marital = $request->marital;
        $user = User::where("id", $user_id)->update(["marital"=>$marital]);
        if($user > 0) return response()->json(['marital'=>$marital], $this->successStatus);
        else return response()->json(["error"=>"Can not update marital."], 500);
    }

    public function update_spiritual(Request $request){
        $user_id = $request->user_id;
        $spiritual = $request->spiritual;
        $user = User::where("id", $user_id)->update(["spiritual"=>$spiritual]);
        if($user > 0) return response()->json(['spiritual'=>$spiritual], $this->successStatus);
        else return response()->json(["error"=>"Can not update spiritual."], 500);
    }

    public function update_alcohol(Request $request){
        $user_id = $request->user_id;
        $alcohol = $request->alcohol;
        $user = User::where("id", $user_id)->update(["alcohol"=>$alcohol]);
        if($user > 0) return response()->json(['alcohol'=>$alcohol], $this->successStatus);
        else return response()->json(["error"=>"Can not update alcohol."], 500);
    }
    public function update_tobacco(Request $request){
        $user_id = $request->user_id;
        $tabacco = $request->tabacco;
        $user = User::where("id", $user_id)->update(["tabacco"=>$tabacco]);
        if($user > 0) return response()->json(['tabacco'=>$tabacco], $this->successStatus);
        else return response()->json(["error"=>"Can not update alcohol."], 500);
    }
    public function update_friend420(Request $request){
        $user_id = $request->user_id;
        $friend420 = $request->friend420;
        $user = User::where("id", $user_id)->update(["friendly420"=>$friend420]);
        if($user > 0) return response()->json(['friendly420'=>$friend420], $this->successStatus);
        else return response()->json(["error"=>"Can not update friendly420."], 500);
    }
    public function update_children(Request $request){
        $user_id = $request->user_id;
        $children = $request->children;
        $user = User::where("id", $user_id)->update(["children"=>$children]);
        if($user > 0) return response()->json(['children'=>$children], $this->successStatus);
        else return response()->json(["error"=>"Can not update children."], 500);
    }
    public function update_animal(Request $request){
        $user_id = $request->user_id;
        $animal = $request->animal;
        $user = User::where("id", $user_id)->update(["animal"=>$animal]);
        if($user > 0) return response()->json(['animal'=>$animal], $this->successStatus);
        else return response()->json(["error"=>"Can not update animal."], 500);
    }

    public function update_user_identify(Request $request){

        $user_id = $request->user_id;
        $identify_id = $request->identify_id;
        $user = User::where("id", $user_id)->update(['identify_id'=>$identify_id]);
        if($user > 0) return response()->json(['identify_id'=>$identify_id], $this->successStatus);
        else return response()->json(["error"=>"Can not update identify."], 500);
    }


    public function update_bio(Request $request){
        try{
            $param_id = $request->param_id;
            $bio = $request->bio;
            $user_id = $request->user_id;
            $param = Param::where("id", $param_id)->update(["bio"=>$bio]);
            if($param > 0){
                $params = Param::where("user_id", $user_id)->get();
                return response()->json(['params'=>$params], $this->successStatus);
            }
            else return response()->json(["error"=>"Can not update bio."], 500);
        }
        catch(Exception $e){
            die(print_r($e));
        }

    }

    public function update_playground(Request $request){
        $user_id = $request->user_id;
        //$romance = $request->romance;
      //  $networking = $request->networking;
       // $friends = $request->friends;
        $romanceEnable = $request->romanceEnable;
        $networkingEnable = $request->networkingEnable;
        $friendsEnable = $request->friendsEnable;
        $user = User::where("id", $user_id)->update(['romance_enable'=>$romanceEnable, "networking_enable"=>$networkingEnable, "friends_enable"=>$friendsEnable]);
        if($user > 0) return response()->json(['romance_enable'=>$romanceEnable, "networking_enable"=>$networkingEnable, "friends_enable"=>$friendsEnable], $this->successStatus);
        else return response()->json(["error"=>"Can not update Playground."], 500);
    }

    public function update_username(Request $request){
        $user_id = $request->user_id;
        $username = $request->username;
        $user = User::where("id", $user_id)->update(['username'=>$username]);
        if($user > 0) return response()->json(['username'=>$username], $this->successStatus);
        else return response()->json(['error'=>"Can not update User name"]);

    }

    public function update_user_distance(Request $request){
        $user_id = $request->user_id;
        $param_id = $request->param_id;
        $distance = $request->distance;
        $param = Param::where("id", $param_id)->update(['distance'=>$distance]);
        if($param > 0){
            $params = Param::where("user_id", $user_id)->get();
            return response()->json(['params'=>$params], $this->successStatus);
        }
        else return response()->json(['error'=>"Can not update Distance"], 500);
    }

    public function update_user_age_range(Request $request){
        $user_id = $request->user_id;
        $param_id = $request->param_id;
        $ages = $request->ages;
        $param = Param::where("id", $param_id)->update(['ages'=>$ages]);
        if($param > 0){
            $params = Param::where("user_id", $user_id)->get();
            return response()->json(['params'=>$params], $this->successStatus);
        }
        else return response()->json(['error'=>"Can not update ages"], 500);
    }

    public function update_user_phone_number(Request $request){
        $user_id = $request->user_id;
        $phone = $request->phone;
        $user = User::where("id", $user_id)->update(['phone'=>$phone]);
        if($user > 0) return response()->json(['phone'=>$phone], $this->successStatus);
        else return response()->json(['error'=>"Can not update phone"]);
    }

    public function delete_account(Request $request){
        $user_id = $request->user_id;
        $user = User::where("id", $user_id)->forceDelete();
        if($user > 0) return response()->json(['success'=>"success"], $this->successStatus);
        else return response()->json(['error'=>"Can not delete your account"]);
    }


    public function get_industry(){

        $industries = Industry::select(["id", "industry as name"])->get();
        return response()->json($industries);

    }

    public function get_receiver_push_token($username) {

        $user = User::where(['username'=>$username])->first();
        if(!$user) return response()->json(["error"=>"No user"], 500);
        return response()->json(['pushToken'=>$user->push_tokens], $this->successStatus);

    }

    public function get_user($username, $myusername){
        $user = User::where(['username'=>$username])->first();
        if(!$user) return response()->json(['error'=>'No User'], 500);
        $me = User::where(['username'=>$myusername])->first();

        $my_position = json_decode($me->position);
        $p1 = $my_position;
        $p2 = json_decode($user->position);
        //     // $R = 2460; // Earth’s mean radius in mile
		//   	// $dLat = $p2->lat - $p1->lat;
		//   	// $dLong = $p2->lng - $p1->lng;
        //     // $a = pow(cos($p2->lat) * sin($dLong), 2) + pow(cos($p1->lat) * sin($p2->lat) - sin($p1->lat) * cos($p2->lat) * cos($dLong), 2);
        //     // $b = sin($p1->lat) * sin($p2->lat) + cos($p1->lat) * cos($p2->lat) * cos($dLong);
        //     // $angle = atan2(sqrt($a), $b);
        //     // $d = intval($R * $angle);
        $latitude1 = $p1->lat;
        $latitude2 = $p2->lat;
        $theta = $p1->lng - $p2->lng;
        $distance = (sin(deg2rad($latitude1))*sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1))*cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;
        $user->dis = intval($distance);
        return response()->json(['user' => $user], $this->successStatus);
    }

    public function block_user($id, Request $request){
        $blocked_id = $request->id;
        $user = User::find($id);
        $blocked_user = User::find($blocked_id);
        $blocked_users = $user->blocked_users;
        if($blocked_users == NULL) $blocked_users = [];
        else $blocked_users = json_decode($blocked_users);
        if(in_array($blocked_id, $blocked_users)){
            return response()->json(['success'=>"success"], 200);
        }
        array_push($blocked_users, $blocked_id);
        $user->update(['blocked_users'=>json_encode($blocked_users)]);
        Http::post(config('app.cloud_url').'blockMessages', [
            'username1' => $user->username,
            'username2' => $blocked_user->username
        ]);
        return response()->json(['success'=>"success"], 200);
    }

    public function get_blocked_users($id){
        $user = User::find($id);
        $blocked_user_ids = $user->blocked_users;
        $blocked_users = [];
        if($blocked_user_ids == NULL) $blocked_user_ids = [];
        else $blocked_user_ids = json_decode($blocked_user_ids);

        foreach($blocked_user_ids as $bid){
            $one = User::find($bid);
            $photos = $one->photos;
            $data = [
                "id" => $one->id,
                "firstname" => $one->firstname,
                "lastname" => $one->lastname,
                "avatar" => ($photos != "[]" && $photos != NULL && $photos != "")?json_decode($photos)[0]:""
            ];
            $blocked_users[] = $data;
        }

        return response()->json(['blocks' => $blocked_users]);
    }

    public function unblock_user($id, Request $request){
        $data = $request->validate([
            'user_id' => 'required'
        ]);
        $user = User::find($id);
        $blocked_user = User::find($data['user_id']);
        $blocks = $user->blocked_users;
        if($blocks == "" || $blocks == "[]" || $blocks == NULL ){
            return response()->json(['success' => "fail"], 200);
        }
        $blocks = json_decode($blocks);
        $key = array_search($data['user_id'], $blocks);
        if($key === FALSE){
            return response()->json(['success'=>"fail"], 200);
        }
        // unset($blocks[$key]);
        $new_blocks = [];
        foreach($blocks as $block){
            if(intval($block) != $data['user_id']) array_push($new_blocks, $block);
        }
        $user->update(['blocked_users' => json_encode($new_blocks)]);

        // Update the firebase messages
        Http::post(config('app.cloud_url').'unblockMessages', [
            'username1' => $user->username,
            'username2' => $blocked_user->username
        ]);

        return response()->json(['success' => "success"], 200);

    }


}
