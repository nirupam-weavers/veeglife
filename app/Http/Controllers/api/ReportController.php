<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\api\BaseController as BaseController;
use App\Report;

class ReportController extends BaseController
{
    //

    public function report(Request $request){
        $data = $request->validate([
            'reporter_id' => "required|integer",
            'user_id' => "required|integer",
            "content" => "required|string"
        ]);
        Report::create($data);
        return response()->json(['success'=>"success"], 200);
    }
}
