<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Http;


class UserController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::all();
        return view('users.users',["users"=>$users]);
    }

    public function user($username){
        $user = User::with("identifies")
                    ->with("astrological")
                    ->with("maritals")
                    ->with("spiritual_belifes")
                    ->with("four20")
                    ->with("have_children")
                    ->with("have_animal")
                    ->with("tobaccos")
                    ->with("params")
                    ->where(["username"=>$username])->first();
        return view('users.profile',['user'=>$user]);
    }

    public function remove($username){
        $result = User::where(['username'=>$username])->delete();
        return back();
    }

    public function messages($username){
        $response = Http::post(config('app.cloud_url').'getMessagesByUser', [
            'username'=>$username
        ]);
        $messages = $response->json();
        $user = User::where(['username'=>$username])->first();
        return view("users.messages", ['messages'=>$messages, 'user'=>$user]);
    }

    public function block_user($id){

        // Set block field to 1

        $user = User::find($id);
        $user->update(['blocked' => 1]);

        // Send FCM to the User as terminated account

        $deviceToken = $user->push_tokens;
        $type="blocked";
        $title="Account terminated!";
        $body = "Sorry, Your account has been terminated!";

        $response = Http::post(config('app.cloud_url').'sendPushNotification',[
            'deviceToken'=>[$deviceToken],
            'type' => $type,
            "title" => $title,
            "body" => $body
        ]);

        return back();

    }

    public function unblock_user($id){

        User::find($id)->update(['blocked' => 0]);

        return back();

    }
}
