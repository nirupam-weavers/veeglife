<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Packages.packages');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array();
        if($request->option=='option1'){
            $data['photo']  = 1;
            $data['tossed']  = 1;
            $data['rewind']  = 1;
            $data['video']  = 0;
            $data['filter']  = 0;

            DB::table('users')->update($data);
            return back()->withToastSuccess('Package Created Successfully!');
        }
        elseif ($request->option=='option2') {
            $data['photo']  = 0;
            $data['tossed']  = 0;
            $data['rewind']  = 0;
            $data['video']  = 1;
            $data['filter']  = 0;
            DB::table('users')->update($data);
            return back()->withToastSuccess('Package Created Successfully!');
        }
        elseif ($request->option=='option3') {
            $data['photo']  = 1;
            $data['tossed']  = 1;
            $data['rewind']  = 1;
            $data['video']  = 1;

            DB::table('users')->update($data);
            return back()->withToastSuccess('Package Created Successfully!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::find($id)->delete();

        return back();
    }
}
