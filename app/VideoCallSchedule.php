<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCallSchedule extends Model
{
    //
    protected $table = "video_call_schedule";
    protected $guarded = [];

    public function caller() {
        return $this->hasOne("App\User", "id", "caller");
    }

    public function receiver(){
        return $this->hasOne("App\User", "id", "receiver");
    }
}
