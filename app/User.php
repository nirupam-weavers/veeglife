<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname','username', 'email', 'password','phone','city','country','birthday','position','photo','video','filter','tossed','rewind'
    ];

    protected $guarded = [''];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function params() {
        return $this->hasMany("App\Param");
    }

    public function identifies() {
        return $this->hasOne("App\Identify", "id", "identify_id");
    }

    public function astrological() {
        return $this->hasOne("App\Astrological", "id", "astrological_id");
    }

    public function maritals(){
        return $this->hasOne("App\Marital", "id", "marital");
    }
    public function spiritual_belifes(){
        return $this->hasOne("App\Spiritual", "id", "spiritual");
    }
    public function alcohols() {
        return $this->hasOne("App\Alcohol", "id", "alcohol");
    }
    public function tobaccos(){
        return $this->hasOne("App\Tabacco", "id", "tabacco");
    }
    public function four20(){
        return $this->hasOne("App\Friend420", "id", "friendly420");
    }

    public function have_children(){
        return $this->hasOne("App\Havechildren", "id", "children");
    }
    public function have_animal(){
        return $this->hasOne("App\Haveanimal", "id", "animal");
    }
}
