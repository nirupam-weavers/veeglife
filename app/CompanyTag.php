<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyTag extends Model
{
    //

    protected $table = "companytags";

    protected $guarded = [''];
}
