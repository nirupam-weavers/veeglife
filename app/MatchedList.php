<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MatchedList extends Model
{
    //
    protected $guarded = [];

    protected $table="matched_list";

    public function tossedUser(){

        return $this->hasOne("App\User", "id", "tossed_id");

    }
}
