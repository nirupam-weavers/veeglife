<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    //

    protected $guarded = ['id'];

    public function reporter(){
        return $this->hasOne("App\User", "id", "reporter_id");
    }

    public function user(){
        return $this->hasOne("App\User", "id", "user_id");
    }
}
